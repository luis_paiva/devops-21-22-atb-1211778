
##Class Assignment 2 -Part1
###Author: Luis Paiva [1211778]

Using Gradle, I used both Windows command line and the intelij terminal.

I started by downloading the project and moving it to a new folder [CA2-Part1] in my Repository.

    mkdir CA2-Part1

![](./images/downloadproject.jpg)

![](./images/createreadme.jpg)

*I changed the name of the readme to readmeCA2part1, when I realized I already had one.*

![](./images/commitandpush.jpg)


I opened the project in Intellij, read and run the instructions on the readme file:

###1.Build

![](./images/buildgradlew.jpg)

###2.Run Server

![](./images/runserver.jpg)

###3.Run Client

![](./images/runClient.jpg)

###Adding a new task to execute the server

To execute the new task, I runned the build first and then executed the command.

![](./images/newTask.jpg)

 To finalize I commited and pushed it to the repository.

![](./images/commitissue20.jpg)

### Add a Junit dependecy, copy the test and run.

![dependecy](./images/junitdependency.jpg)

![TestFolder](./images/testfolder.jpg)

![UnitTest](./images/apptest.jpg)

![runTest](./images/runtest.jpg)



**Add to repository**

![](./images/commitTest.jpg)

![](./images/pushTest.jpg)

### Add a new task of Type Copy, to create a backup folder

![](./images/backupfolder.jpg)

![](./images/copyTask.jpg)

**Add to repository**

![](./images/Addcopy.jpg)

![](./images/pushCopy.jpg)

## Add a new task of the Type Zip.

![](./images/zipFolder.jpg)

![](./images/commit zipFolder.jpg)

* Add to amend my commit, because I used an incorrect comand to close the issue.*

## Place the tag to finish the assignment

    git tag -a ca2-part1 -m "first part"

This completes the first part of the assignment.

![](./images/Issues.jpg)