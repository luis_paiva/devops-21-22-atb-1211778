##Class Assignment 2 -Part2
###Author: Luis Paiva [1211778]


####This Report wil addresse the following issues


![Issues](./part2 images/IssuesOpen.jpg)

    After creating a folder named CA2-Part2 I copied the project into the folder,
    and send it to the repository.

![status](./part2 images/copiedproject.jpg)

![gitadd](./part2 images/add26.jpg)

![commit26](./part2 images/commitissue26.jpg)

![push26](./part2 images/push26.jpg)

    Then I created a new branch to work on.

![newbranch](./part2 images/newbranch.jpg)

    Next I started a new gradle spring boot project at https://start.spring.io:

![springboot](./part2 images/inicializadorspring.jpg)

    And extracted the files to CA2-Part2 folder.

![extratfiles](./part2 images/extractedfiles.jpg)

![springfiles](./part2 images/inicializeronbranch.jpg)


    At this point I commit and push to the repository, solving issues 27, 28 and 29.

![commit](./part2 images/commitissue272829.jpg)

![push](./part2 images/pushnewbranch.jpg)

    Next I run ./gradlew tasks to check the existing tasks.

![tasksone](./part2 images/tasksbootrun.jpg)


![taskstwo](./part2 images/tasksgradle.jpg)
 
![push](./part2 images/pushnewbranch.jpg)

    The next step is to delete the src file and replace it by the one in the
    basic folder, I performed this in Intellij.

![copysrc](./part2 images/Copysrc.jpg)

![commit3031](./part2 images/commit3031.jpg)


    Copied the webpack.config.js and package.json files.  And deleted src/main/resources/static/built/ 
    And here I realized I add closed the wrong Issue (#31).
    So I tried and succeeded to reopen and close the issue in the same commit.

![copywebpackfile](./part2 images/copywebpack.jpg)

![copyjsonfile](./part2 images/copyjson.jpg)

![reopen31](./part2 images/reopen31.jpg)

![push](./part2 images/pushoriginbranch.jpg)

    Run the aplication with the ./gradlew bootRun.

![bootRun](./part2 images/bootRun.jpg)

    This only gives a white page because I still haven´t installed the frontend plugins.
    So I have to add:
    -the gradle plugin org.siouan.frontend
![](./part2 images/siouancorrect.jpg)


![](./part2 images/pluginsioun.jpg)

    -the frontend plugin
![](./part2 images\frontendcode.jpg)

    -Update the scripts

![](./part2 images/scripts.jpg)

    -And now I can run the aplication.

![](./part2 images/buildfrontend.jpg)

![](./part2 images/buildfrontendcheck.jpg)

    I can see the aplication running

![](./part2 images/localhost.jpg)

    And I commit and push.

![](./part2 images/commit323334.jpg)

![](./part2 images/pushoriginbranch.jpg)


####Adding tasks

    The first task must be able to copy the .jar files into a dist folder.

![](./part2 images/taskjar.jpg)

![](./part2 images/runcopyJar.jpg)

![](./part2 images/taskjarterminal.jpg)

![](./part2 images/commit35.jpg)

![](./part2 images/pushoriginbranch.jpg)

    The second task is a delete task that runs before the clean task.

![](./part2 images/taskdelete.jpg)

    Here we can see the created tasks. I used the println just to complete the request 15
    Experiment all the developed features. 

![](./part2 images/otherTasks.jpg)

    
![](./part2 images/commit36.jpg)

![](./part2 images/pushoriginbranch.jpg)

    With all the tasks running, I can merge the branch with the master.
![](./part2 images/merge.jpg)

![](./part2 images/mergedone.jpg)

![](./part2 images/commit37.jpg)

##Class Assignment 2 -Part2 Alternative - Maven

The alternative using Maven, we only have to performe the tasks, since the basic project is already
prepared to run the tool.

After coping the project to a folder (CA2-Part2Alternative), following the same steps as in the first part,
I started by running:

![](./images maven/maveninstall.jpg)

the command ./mvnw install is the equivalent to ./gradlew build 

Then I run the aplication:

![](./images maven/bootrunmaven.jpg)

./mvnw spring-boot: run is the equivalent to ./gradlew bootrun

While in Gradle we have tasks, with have plugins and we write the code in the pom.xml file .

![](./images maven/plugins.jpg)

It was harder to add the plugin to copy the jar files, but not so much to add the delete function.
But only because I already knew what to look for, because I thought it was very similar with gradle.

*Plugin to copy jar files*

![](./images maven/copyjar.jpg)

*Created directory dist*

![](./images maven/runcopy.jpg)

![](./images maven/commitcopy.jpg)

*Plugin to delete the files*

![](./images maven/cleanplugin.jpg)

![](./images maven/cleanmaven.jpg)

![](./images maven/commitdelete.jpg)
    
| Graddle           | Maven                   |
|-------------------|-------------------------|
| ./gradlew build   | ./mvnw install          |
| ./gradlew bootrun | ./mvnw spring-boot: run |
| Tasks             | Plugins                 |
| build.gradle      | pom.xml                 |
| includes Wrapper  | includes wrapper        |

In conclusion, Gradle is far easier to use, like we talked during classes, is more user friendly then Maven.
Much more intuitive, for someone with little experience using either of these tools Gradle is a simpler solution.

    
