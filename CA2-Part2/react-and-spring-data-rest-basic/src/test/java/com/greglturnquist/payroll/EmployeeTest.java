package com.greglturnquist.payroll;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    String one_firstName;
    String one_lastName;
    String one_description;


    String two_firstName;
    String two_lastName;
    String two_description;



    @BeforeEach
    public void init() {

        // Default values employee 1
        one_firstName = "Frodo One";
        one_lastName = "Baggins";
        one_description = "ring bearer";


        // Default values employee 2
        two_firstName = "Joe";
        two_lastName = "Doe";
        two_description = "example";


    }
    @Test
    void shouldReturnTrue_createNewEmployee() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description);
        Employee employeeTwo = new Employee(one_firstName, one_lastName, one_description);

        // assert
        assertEquals(employeeOne, employeeTwo);
    }

    @Test
    void shouldReturnTrue_createNewEmployeeHashCode() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description);
        Employee employeeTwo = new Employee(one_firstName, one_lastName, one_description);

        // assert
        assertEquals(employeeOne.hashCode(), employeeTwo.hashCode());
    }

    @Test
    void shouldReturnFalse_createNewEmployee() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description);
        Employee employeeTwo = new Employee(two_firstName, two_lastName, two_description);

        // assert
        assertNotEquals(employeeOne, employeeTwo);
    }
}