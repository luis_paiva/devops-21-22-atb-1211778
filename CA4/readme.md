## Class Assignment 4 Part1

For this assignment I will use Docker to create image and a container.

This first part as two requests.

In the first request I will clone the aplication to the container and run the Server from it.
In the second request I will run the aplication in my computer from inside the container.

First I downloaded the docker desktop from https://www.docker.com/products/docker-desktop.
SInce I don´t have hyper-v I downloaded WSL2 from https://docs.microsoft.com/en-us/windows/wsl/install and installed Ubuntu 18.04.

    wsl --install -d Ubuntu-18.04

Downloaded the updates, and set wsl2 as defaul:

    wsl update

    wsl --set-default-version 2

I created a file called dockerfile into the folder CA4/Part1 in my repository.

In this file I placed the following command lines, in the order I wanted them to run:

    FROM ubuntu:18.04

Because I want to run the aplication from a Ubuntu OS.

    RUN apt-get update
    RUN apt-get install
    RUN apt-get clean

To install any missing components to Ubuntu.


    RUN apt-get install -y git
    RUN apt-get install -y openjdk-8-jdk-headless

Since I will need to clone the aplication I needed to install git, and since it runs in java I installed
open jdk 8.

    RUN git clone https://Luis_Paiva:ZSZgXf6n3mzyqmjyt5Kd@bitbucket.org/luis_paiva/devops-21-22-atb-1211778.git

This is to clone my repository into the container.

    WORKDIR /devops-21-22-atb-1211778/CA2-Part1/luisnogueira-gradle_basic_demo-d8cc2d7443c5

I placed the path to where I have my dockerfile.

    RUN chmod u+x gradlew

Run chmod to give permissions to execute gradle


    RUN ./gradlew clean
    RUN  ./gradlew build

Build the aplication.

    ENTRYPOINT  java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
    EXPOSE  59001

And run the aplication, through port 59001.

To create the image and the container I openned the command line to the Part1 folder and typed the follwing commands:

    docker build runserver .

    docker run -p 59001:59001 --name runserver -d runserver

After running the container I placed a tag and pushed the image file to docker hub, on a account I previously created.

        docker login

        docker tag runserver:latest paivaluis/runserver:ca4-part1

        docker push paivaluis/runserver:ca4-part1

For the second request, I created a new folder to where I copied the jar file, and created a new docker file.

In this new file I first had to change the ubuntu for openjdk:11, for some reason it wouldn´t run otherwise.

    FROM openjdk:11

Next, since I copied the jar file to the folder where I had my docker file, I placed the working directory as root.

    WORKDIR /root

And copied the jar file into the container.


    COPY basic_demo-0.1.0.jar .

To finish I made available the same port as before

    EXPOSE 59001

    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

After I tested this, I tagged and pushed the file to Docker Hub.

    docker login

    docker tag jarfile:latest paivaluis/jarfile:requesttwo

    docker push paivaluis/jarfile:requesttwo

This ended the CA4-part1 assignment.

    

