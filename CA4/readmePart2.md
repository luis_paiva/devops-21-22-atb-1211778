## Class Assignment 4 Part2 ##

In the second part of this assignment I created a docker-compose file to run two different containers.

I created a folder to the second part of the assignment (Part2), with a docker-compose.yml file and a folder for the web container and another for the db container. For each container I created a dockerfile.

I used the docker-compose file from docker-compose-spring-tut-demo project as base to create my own file.

I only changed: the IP adresses from 192.168.33.10 and 192.168.33.11, for 192.168.56.10 and 192.168.56.11, and the subnet config to 192.268.56.00/24.

Then I created two dockerfiles for my images:

* For my db container

````
FROM ubuntu:18.04

 RUN apt-get update
 RUN apt-get install -y openjdk-8-jdk-headless
 RUN apt-get install unzip -y
 RUN apt-get install wget -y

 RUN mkdir -p /usr/src/app

 WORKDIR /usr/src/app/

 RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

````

Installed ubuntu 18.04, because I wanted to use jdk11, but the first time I tried to run dock-compose it stopped in the openjdk-11-jdk-headless, tried jdk8 and worked.

This the only problem I had in this dockerfile.

* For my web image

````

FROM tomcat:9-jdk11-openjdk-slim

RUN apt-get update -y 
RUN apt-get install -f
RUN apt-get install git -y
RUN  apt-get install nodejs -y
RUN apt-get install npm -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build
WORKDIR /tmp/build/

RUN git clone https://Luis_Paiva:ZSZgXf6n3mzyqmjyt5Kd@bitbucket.org/luis_paiva/devops-21-22-atb-1211778.git

 WORKDIR /tmp/build/devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic

 RUN chmod u+x gradlew

RUN ./gradlew clean
RUN ./gradlew build
RUN  cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -Rf /tmp/build/

EXPOSE 8080

``````

As the teacher suggested I used tomcat:9-jdk11-opensdk-slim (VsCode gived some options for jdk, I choosed slim expecting it to be smaller, don´t think it was).

Cloned my repository from CA3-Part2, added the chmod command for permissions, swapped the basic-0.0.1-SNAPSHOT.war for the react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war.

Opened a command line and run :

        docker-compose up


![](./images/containerpart2.jpg)


Checked the name of the images that I wanted to tag and push by using:

    docker-compose images

And send them to the Docker Hub:

    docker tag part2_db:latest paivaluis/part2_db:latest


    docker push paivaluis/part2_db:latest


    docker tag part2_web:latest paivaluis/part2_web:latest

    
    docker push paivaluis/part2_web:latest


For the last request to Use a volume with the db container to get a copy of the database file by using the
exec to run a shell in the container and copying the database file to the volume.

First I used the command:

    docker-compose exec db sh

Inside the container used:

        cp /usr/src/app/jpadb.mv.db /usr/src/databackup

Sending the file to the data folder in my computer.