## Class Assignment 5 Part 2

###Author: Luis Paiva [1211778]

The purpose of the second part of the assignment was to in addition to the stages implemented in part 1,
to add a javadoc stage and a Docker image stage(where I create a Docker image, and send it to my DockerHub repository).

![](./Images/stages.jpg)

So I started Jenkins like in part 1 of the assignment running in a terminal:

    java -jar jenkins.war

First I installed the plugins I needed for the new stages.

For the javadoc stage:
![](./Images/javadoc plugin.jpg)

![](./Images/htmlpublisherplugin.jpg)

And for the Docker Image stage:

![](./Images/dockerplugins.jpg)

Then I created a Dockerfile to add to the CA3/Part2/react-and-spring-data-rest-basic folder in the repository:


    FROM tomcat:8-jdk11-temurin

    RUN apt-get update -y

    RUN apt-get install sudo nano git nodejs npm -f -y


    RUN apt-get clean && rm -rf /var/lib/apt/lists/*

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://Luis_Paiva@bitbucket.org/luis_paiva/devops-21-22-atb-1211778.git

    WORKDIR /tmp/build/devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic

    ADD ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8085

I used the port 8085, because I tought that if I was using 8080 for jenkins, I should use another.


After this I started a knew job, and created the new scripts for all stages.

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://Luis_Paiva@bitbucket.org/luis_paiva/devops-21-22-atb-1211778'

                    }
                }
            stage('Assemble') {
                steps {
                    dir ("CA3/Part2/react-and-spring-data-rest-basic"){
                    echo 'Assembling...'
                    bat './gradlew assemble'
                    }
                }
            }
            stage('Test') {     
                steps {
                    dir ("CA3/Part2/react-and-spring-data-rest-basic"){
                    echo 'Testing...'
                    bat './gradlew test'
                    junit 'build/test-results/test/*.xml'
                    }
                }
            }
            stage ('Javadoc'){
                steps{
                    dir("CA3/Part2/react-and-spring-data-rest-basic"){
                    echo 'Publishing...'
                    bat './gradlew javadoc'
                    publishHTML([allowMissing: false,
                    alwaysLinkToLastBuild: true,
                    keepAll: true, reportDir: 'build/docs/javadoc',
                    reportFiles: 'index.html', reportName: 'HTML Report',
                reportTitles: ''])

            }
      }
    }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    dir ("CA3/Part2/react-and-spring-data-rest-basic"){
                    archiveArtifacts 'build/libs/*'
            }
        }
    }
            stage ('Docker Image') {
                steps {
                    echo 'Publishing...'
                    dir("CA3/Part2/react-and-spring-data-rest-basic") {

            script {
                docker.withRegistry('https://registry.hub.docker.com/', 'docker_credentials') {

        def customImage = docker.build("paivaluis/image:${env.BUILD_ID}")



                customImage.push()
                       }
                  }
              }
          }
      }
        }
      }
For the javadoc stage I went to the pipeline Syntax and used publish html. but I am not sure it was
the correct way to do it, when I open  the tab to see the result I get the following:

![](./Images/htmltab.jpg)

![](./Images/htmlreport.jpg)

I added my Docker hub credentials to jenkins with the name docker_credentials and placed them in the script,
I also needed to had my username to the docker build command and the Buil number with env.BUIL_ID to tag
with this number.

![](./Images/dockerhubjenkins.jpg)


And this way I had a sucessfull build in my pipeline... after 26 failures!

![](./Images/stages.jpg)

Implementing a alternative

After talking with some off my classmates that said Buddy was a good option and easy to use,
I decided to use this.

    https://app.buddy.works/1211778

I created a new repository for this and started a new pipeline:

![](./Images/buddy pipeline.jpg)

In buddy we create actions and not stages, and it has a really friendly interface.

![](./Images/actions.jpg)

All the commands for gradle were placed in the gradle action, and the docker image,
and the push of the image add to be done in different actions.

![](./Images/dockerhubbuddy.jpg)

I could not take more images, because I toke some time to present my report, and I was using a free
version of buddy that expired.

But I found it very easy to use, and only toke me four attempts to build the pipeline.

This is the last assignment, and I would like to say that I really enjoyed them, and the way the classes
are presented. in some of the assignments I wish i could have had more time to explore the softwares
presented, but Im still strugling in some of the other classes, and I couldn´t take has much time has I wanted
with Devops but I really enjoyed it.

Thank you.





