## Class Assignment 5 Part 1

###Author: Luis Paiva [1211778]

####What is Jenkins?

Jenkins is an open source continuous integration/continuous delivery and deployment (CI/CD) automation software DevOps tool written in the Java programming language. It is used to implement CI/CD workflows, called pipelines.

- Installing jenkins:

I started by downloading the LTS generic java package from https://www.jenkins.io/download/

-Running jenkins

To run directly the war file I executed the command:

    java -jar jenkins.war


After running the command I acessed jenkins through http://localhost:8080 and finished the setup, wich included the necessary plugins.

![](./Images/Captura%20de%20ecr%C3%A3%202022-05-26%20143646.jpg)

![](./Images/Captura%20de%20ecr%C3%A3%202022-05-26%20144924.jpg)

![](./Images/entry.jpg)

![](./Images/plugins.jpg)

- Create a new pipeline
  
Entering jenkins, I created a new item on the the dashboard and chose the pipeline option.


![](./Images/newitem.jpg)

- Pipeline Script

The first script was inserted in the item configuration page.

![](./Images%5Cscriptca5.jpg)
![](./Images/script%20ca52.jpg)

The other way to run jenkins is through Pipeline script from SCM instead of Pipeline script.

![](./Images/scriptca5part1.jpg)

![](./Images/scriptca5part1two.jpg)

First I created the credencials with:

    ssh-keygen


![](./Images/keygen.jpg)


The keys are stored here :

![](./Images%5Csshkeys.jpg)

In the item configuration page, add credentials and copy the public key. In your repository add the private key.

In jenkins file added the name I gave to the credentials, in the field credentialsId.  



On both scripts we have the same stages.

- Stage Checkout - clone the repository; 

- Stage Assemble - Compiles and Produces the archive files with the application;

- Stage Test - Executes the Unit Tests and publish in Jenkins the Test results;

- Stage Archiving - Archives in Jenkins the archive files (generated during Assemble)

Running build in jenkins I will get:

![](./Images/sucessScript.jpg)


![](./Images/build.jpg)


In the left side of the build page, I have the test results.

![](./Images/testmenu.jpg)

![](./Images/testresults.jpg)
