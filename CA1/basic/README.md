
# **Technical report of CA1 assignment of Devops class.**

#### **Author: Luis Paiva [1211778]**

##**Introduction**

This report will show the steps taken to complete the first assignement of Devops class.
The topic of the assignment is to aply the use of the Version Control System (vcs) GIT, 
using the repository created in the first class at (https://bitbucket.org) and compare to a alternative of my choice.
My first choice for alternative was a centrallized VCS called SLik Subversion (https://sliksvn.com)
and  the use of the Sourcefourge repository (https://sourceforge.net),
but some difficulties uploading the project, forced me to choose a different alternative.
Therefore, the alternative to be presented in this assignement is the Mercurial VCS (https://www.mercurial-scm.org),
with use of the Helixteamhub repository (https://helixteamhub.cloud).


##Using GIT

**I started by creating the assignment empty folder by using the command line:**

    mkdir CA1

![create folder](./reportImages/createfolder.jpg)

Then i copied the tutorial into this folder in windows.

I passed the modified files to the stagging area with the command:

    git add .

Performed the first commit:

    git commit

![first commit](./reportImages/first commit.jpg)


And the first push:

    git push

![first push](./reportImages/gitpush.jpg)


###Note:
            the #1 that appears in the message of the first commit points to the issues created in Bitbucket, these
        work has references to the tasks to be executed in the assignment.

![](./reportImages/issues.jpg)

**First mistake**

My first mistake was when i had to implement the use of tags.
I used the correct command to create the tag:

    git tag -a v1.1.0 -m "Inicial Version"

But i thought that by doing push to the tag I was also pushing my previous commit, so I used:

    git push origin v1.1.0

before pushing my initial commit.

The following commits refer to the changes requested to the aplication.

- Add a new field "jobYears";
            
- Add tests add unit tests for testing the creation of Employees and the validation
  of their attributes (for instance, no null/empty values). For the new field, only integer
  values should be allowed.

    - I created validations for all the atributes and applied them directly on the construcor, 
to be validated at the creation (I runned all the tests and debugged the app) .

![](./reportImages/validation.jpg)

At this point I made a new commit to close issues #7 and #9, wich resolved in my second mistake.


Although this time i commited and pushed the changes before creating the V1.2.0 tag, when creating the tag
I forgot to add a message, and that opened automatically the editor so that i could write one. 

Inicially the README.md file add been created in this path C:\GIT\Devops\devops-21-22-atb-1211778\CA1>, and I transfered
it to C:\GIT\Devops\devops-21-22-atb-1211778\CA1\basic> with:

    mv README.md ..\CA1\basic

At this point the app is in version 1.2.0

**git log**

    git log --oneline --decorate --graph --all 

![git log](./reportImages/gitlog v1.2.0.jpg)

##Part Two

**Create a new branch.

To add a new branch as requested, I used the following commands:

-To create the branch;

     git branch email-field

-To work on the new branch;

    git checkout email-field;

![](./reportImages/gitcheckout.jpg)

![](./reportImages/gitbranch.jpg)


After adding a new email field, I added the changes I made in the branch to the stagging area, commited and pushed them.

    git add .

    git commit -m "Resolves #12 new field and tests"

    git push origin email-field

![Bitbuket Branchs](./reportImages/branches.jpg)

![](./reportImages/Apontador.jpg)

After this I merged  the email-field branch with master, and deleted the branch.

    git merge email-field

![](./reportImages/merge.jpg)

![](./reportImages/tag1.3.jpg)

![](./reportImages/branchdelete.jpg)

Leaving only the master branch again.

![](./reportImages/bitbranch.jpg)

Next I added the new fix-invalid-email branch:

    git branch fix-invalid-email

    git checkout fix-invalid-email

![](./reportImages/fixemail.jpg)


And added a validation and tests to validate emails.

    git add .

    git commit -m "Resolves #14 and #15"

    git push origin fix-invalid-email

And merged it to main, adding the v1.3.1 tag.

    git merge fix-invalid-email

![](./reportImages/gitmergeinvalidemail.jpg)

    git tag v1.3.1 - m "revised version"

    git push origin master version

![](./reportImages/git131.jpg)
  
**Ending the first part of the assignment.**

##Using mercurial (alternative)

As a alternative to GIT as a Version Control System I used mercurial, also a distributed source control management tool.

Like in GIT I started by creating a empty directory:

    mkdir CA1

After creating a repository in (https://helixteamhub.cloud) I cloned it for my local machine:

    https://1211778isepipppt@helixteamhub.cloud/damp-duck-8532/projects/mercurialca1/repositories/mercurial/devops

After copying the files and since mercurial has no stagging area (one of the first diferences I noticed),
I used the commit comand:


                                        hg commit -m "Initial commit"

Followed by the push, where I had to place the following URL:

     hg push  https://1211778isepipppt@helixteamhub.cloud/damp-duck-8532/projects/mercurialca1/repositories/mercurial/devops

Using tags with mercurial is very similar to GIT, with some slight differences. While using the tag command
for a specific commit (like in GIT):

    hg tag v.1.2.0 -m "Second version" bcd544621d30 
 
I ended by placing two tags in the same commit:

![hstag](./reportImages/hstag.jpg)

One other difference regards the Issues, since I was unable to close or resolve them using the command line.
I had to adresse this in the repository, and close them manually.

**hslog**

  hs log --graph
  
![hs log](./reportImages/hslog.jpg)

The hs log shows more information, namely the tags in the commits.

#Part Two

**Create a new branch.

To add a new branch as requested, I used the following commands:

-To create the branch;

    hg branch email-field

    hg commit -m

    hg push https://1211778isepipppt@helixteamhub.cloud/damp-duck-8532/projects/mercurialca1/repositories/mercurial/devops --new-branch


![](./reportImages/hgemailbranch.jpg)


![](./reportImages/hgpushnewbranch.jpg)


To work in the new branch 

    hg upload email-field

I had some problems with the merges, I think it has to do with the .hgignore, not sure that
I have the correct files in there.

After creating the new field I changed back to the default branch and merged the two.

![](./reportImages/mergehgemailfield.jpg)

Next I created the fix-invalid-email branch

    hg branch fix-invalid-email

    hg commit -m "Resolves #7 new fix-invalid-email branch "

    hg push https://1211778isepipppt@helixteamhub.cloud/damp-duck-8532/projects/mercurialca1/repositories/mercurial/devops --new-branch

Added the new validation, commited and pushed it.

Merged back with the default branch

![](./reportImages/mergefixinvalid.jpg)

And added the tags to finish.

![](./reportImages/hgfinal.jpg)

