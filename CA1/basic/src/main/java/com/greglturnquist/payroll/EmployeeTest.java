package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    @Test
    void validateFirstName() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        String firstName = "Luis";

        String expected = "Luis";
        String result = emp.validateFirstName(firstName);

        assertEquals(expected, result);
    }

    @Test
    void validateFirstName_Null() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee(null, "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateFirstName_Empty() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateLastName() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        String lastName = "Paiva";

        String expected = "Paiva";
        String result = emp.validateLastName(lastName);

        assertEquals(expected, result);
    }

    @Test
    void validateLastName_Null() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", null, "Switch", "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateLastName_Empty() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "", "Switch", "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateDescription() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        String description = "Switch";

        String expected = "Switch";
        String result = emp.validateDescription(description);

        assertEquals(expected, result);
    }

    @Test
    void validateDescription_Null() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", null, "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateDescription_Empty() {

        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "", "Developer", 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateJobTitle() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        String jobTitle = "Developer";

        String expected = "Developer";
        String result = emp.validateJobTitle(jobTitle);

        assertEquals(expected, result);
    }

    @Test
    void validateJobTitle_Null() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", null, 3, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateJobTitle_Empty() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "", 3, "luis@isep.ipp.pt");
                });
    }


    @Test
    void validateJobYears() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        int jobYears = 3;

        int expected = 3;
        int result = emp.validateJobYears(jobYears);

        assertEquals(expected, result);
    }

    @Test
    void validateJobYearsEqualsZero() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 0, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateJobYearsEqualsNegativeNumber() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", -5, "luis@isep.ipp.pt");
                });
    }

    @Test
    void validateEmail() {
        Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 3, "luis@isep.ipp.pt");
        String email = "luis@isep.ipp.pt";

        String expected = "luis@isep.ipp.pt";
        String result = emp.validateEmail(email);

        assertEquals(expected, result);
    }

    @Test
    void validateEmailNull() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 5, null);
                });
    }

    @Test
    void validateEmailEmpty() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 5, "");
                });
    }

    @Test
    void testValidateEmail_InvalidEmail() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Employee emp = new Employee("Luis", "Paiva", "Switch", "Developer", 5, "luisisepipppt");
                });
    }
}