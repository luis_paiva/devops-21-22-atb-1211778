/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;


import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private int jobYears;
    private String email;

    private Employee() {
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {
        this.firstName = validateFirstName(firstName);
        this.lastName = validateLastName(lastName);
        this.description = validateDescription(description);
        this.jobTitle = validateJobTitle(jobTitle);
        this.jobYears = validateJobYears(jobYears);
        this.email = validateEmail(email);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(jobTitle, employee.jobTitle) &&
                Objects.equals(jobYears, employee.jobYears) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description, jobTitle, jobYears, email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    protected String validateFirstName(String firstName) {
        if (firstName != null && !firstName.isEmpty()) {
            return firstName;
        } else {
            throw new IllegalArgumentException("Insert a valid name.");
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    protected String validateLastName(String lastName) {
        if (lastName != null && !lastName.isEmpty()) {
            return lastName;
        } else {
            throw new IllegalArgumentException("Insert a valid name.");
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    protected String validateDescription(String description) {
        if (description != null && !description.isEmpty()) {
            return description;
        } else {
            throw new IllegalArgumentException("Insert a valid description.");
        }
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    protected String validateJobTitle(String jobTitle) {
        if (jobTitle != null && !jobTitle.isEmpty()) {
            return jobTitle;
        } else {
            throw new IllegalArgumentException("Insert a valid job title.");
        }
    }

    public int getJobYears() {
        return jobYears;
    }

    public void setJobYears(int jobYears) {
        this.jobYears = jobYears;
    }

    protected int validateJobYears(int jobYears) {
        if (jobYears > 0) {
            return jobYears;
        } else {
            throw new IllegalArgumentException("Insert a valid number.");
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    protected String validateEmail(String email) {

        if (email == null) {
            throw new IllegalArgumentException("Insert a valid email.");
        }

        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email.trim());

        if (m.find()) {
            return email;
        }

         else {
            throw new IllegalArgumentException("Insert a valid email.");
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", jobYears='" + jobYears + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
// end::code[]
