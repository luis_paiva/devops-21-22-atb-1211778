Class Assigment 3 - Part 2

On this assignment I will use Vagrant to create two virtual machines (db and web).

First I downloaded Vagrant (https://www.vagrantup.com) and installed it in my PC.
Then I checked the version I downloaded.

![](./images-part2/vagrant-v.jpg)

To save some work I copied the Vagrant file from the project:

    https://bitbucket.org/atb/vagrant-multi-spring-tut-demo


 I copied the file to the folder CA3-Part2 in my repository, as well as the react-and-spring-data-rest-basic project
from th second class assignment.

 The first change I had to make in the vagrant file was to swap the box it was being used.
 The box used in the existing file *envimation/ubuntu-xenial* could only be used with java 8 and only with virtual box.
 Since I had created the project using java 11 and I wanted to use vmware has a alternative I had to
 choose a new box - bento/ubuntu-18.04.

![](./images-part2/changebox.jpg)

![](./images-part2/java 11.jpg)

I also changed the box in the configurations specific to the virtual machines.

![](./images-part2/dbbox.jpg)

![](./images-part2/webbox.jpg)


After this I added the necessary commands to run my aplication after the container is installed.

![](./images-part2/clone.jpg)

First used rm -R -f devops-21-22-atb-1211778 , this removes any project that exists before I clone it.

Then I cloned the project to inside container :

    git clone https://Luis_Paiva@bitbucket.org/luis_paiva/devops-21-22-atb-1211778.git

Go to the location where I can build the project after is cloned:

    cd devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic


Give the execute permission to the gradlew file (the version of gradle thas wrapped in th project)

    chmod u+x gradlew

And run gradle:

    ./gradlew clean build

To end, and deploy the war file I run:

    sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

After the Vagrant file was ready I had to change some of the project files.

In .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\src\main\java\com\greglturnquist\payroll, added this
new class:
```

package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```
And updated the following files acording to the vagrant-multi-spring-tut-demo:

* .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\src\main\resources\application properties

* .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\src\main\resources\templates\index

* .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\src\main\js\app.js

* .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\.gitignore

* .\devops-21-22-atb-1211778/CA3/Part2/react-and-spring-data-rest-basic\settings.gradle

   
With the changes made I was able to run the project:

    vagrant up

Being able to make a sucessfull build, I visited the following adresses:

* To open the web aplication:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

![](./images-part2/localhostreact-and-spring-data-rest-basicSNAPSHOT.jpg)

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

![](./images-part2/19216856108080react-and-spring-data-rest-basicSNAPSHOT.jpg)

* To open the h2 console:

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

![](./images-part2/localhostdatabasestring.jpg)

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console

![](./images-part2/IPdatabaseinside.jpg)


As an alternative I intended to use Vmware, and I changed the vagrant file to do so.

Kept the box, because it could be used by multiple providers.

Changed the the configurations specific to the virtual machines, with vmware_desktop.
Changed the IP adresses in the private network.

But when I used the command 

  vagrant up --provider=vmware_desktop
  
I came across a error that I could not solve.

![](./images-part2/MicrosoftTeams-image.png)

I tried solving it in several ways, but was unable to do so.