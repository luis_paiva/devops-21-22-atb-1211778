##Class Assignment 3 -Part1

###Author: Luis Paiva [1211778]

This third assignment adresses the use of VirtualBox a virtualization software
to run the projects from our previous works in a Ubuntu enviroment.

First, I downloaded Virtual Box from the link 
*https://www.virtualbox.org/.

![](./imagens/virtualBox.jpg)

After this I created a Virtual Machine like described in the class pdf:

 - Downloaded Ubuntu 64bits minimal ISO file from
*https://help.ubuntu.com/community/Installation/MinimalCD*

![](./imagens/iso.jpg)

- Made available 2gb RAM and 10GB of disk space.

![](./imagens/createdMachine.jpg)

- And setted to Network Adapters.

![](./imagens/adaptorHost.jpg)

- I started the VM and logged in ubuntu to continue the setup:

- Updating the packages:


    sudo apt update

![](./imagens/package.jpg)

- Installed the network tools:


    sudo apt install net-tools

![](./imagens/networkTools.jpg)

- And editted the network configuration file to setup the IP:


    sudo nano /etc/netplan/01-netcfg.yaml

![](./imagens/netplan.jpg)

*The jpg has a mistake I incorrectly wrote emp0s8 then replaced it by enp0s8.*

- Applied the changes


    sudo netplan apply

- Installed the openssh-server so that I could use ssh to open secure terminal sessions.


    sudo apt install openssh-server

- Enabled password authentication for ssh by entering the sshd_config file through the nano editor:


    sudo nano /etc/ssh/sshd_config

- uncommented the line PasswordAuthentication yes
- Restarted the server


    sudo service ssh restart

- Installed a ftp server so that I could use the FTP protocol to transfers files to/from
the VM (from other hosts)


    sudo apt install vsftpd


    sudo nano /etc/vsftpd.conf

- Enabled write access for vsftpd
- uncommented the line write_enable=YES, in the vsftpd.conf file

- Restarted the ftp server

- Opened a terminal to use ssh to connect to the VM


    ssh luis@192.168.52.5



- The next step was to install and run FileZilla:
*https://filezilla-project.org/*

![](./imagens/filezilla.jpg)


- Installed git and java 11


    sudo apt install git


    sudo apt install openjdk-8-jdk-headless


- At this point I could clone the repository to my VM using:

*git clone https://Luis_Paiva@bitbucket.org/luis_paiva/devops-21-22-atb-1211778.git*

- Build the basic project with maven from Class Assignment I using:

        mvn spring-boot:run

![](./imagens/buildmaven.jpg)

- Opened the application using the browser in my host computer with the correct url:

![](./imagens/basicMavenCA1.jpg)


- To performe the tasks from Class Assignment II part 1

Tryed to do the build, but didn´t work, had to give permission to execute first, using:

    chmod u+x gradlew

- After this I could do the build


    ./gradlew build

- Before running the aplication I had to change the runClient task in build.gradle,
replacing localhost by the IP from my adaptor 192.168.56.5.

![](./imagens/runclient.jpg)

- As instructed I run the taskRunServer from the VM, since it doesn´t as graphic interface


    ./gradlew runServer

- Openned a terminal and connected with ssh to the VM to run the client:


    ssh luis@192.168.52.5


    -/gradlew runClientVM

![](.\imagens\runclientbash.jpg)

    
![](.\imagens\runserverluis.jpg)

- To finalize, the Class assignment the class assignment 2 Part 2. consists on running the basic project
with gradle.


    ./gradlew build


    ./gradlew bootRun


![](./imagens/gradlebootRungradle.jpg)


- And open the browser with the URL http://192.168.56.5:8080/

![](./imagens/gradlebootrun.jpg)

- And this completes the first part of class assignment 3.